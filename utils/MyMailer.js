const fs = require('fs');
const ejs = require('ejs');
const nodemailer = require('nodemailer');

const env = process.env.NODE_ENV || 'local';
const config = require('../keys/config.json');

class MyMailer {
  constructor(member, subject, password) {
    this.member = member;
    this.subject = subject;
    this.password = password;
  }

  async send() {
    console.log('==> SEND MAIL TO', this.member.email);
    if (this.member.email) {
      nodemailer.createTestAccount((err, account) => {
        const transporter = nodemailer.createTransport({
          host: 'smtp.googlemail.com',
          port: 465,
          secure: true,
          auth: {
            user: 'zainudinkusuma@gmail.com',
            pass: 'wgsejwmiapvljfmo',
          },
        });
        const mailOptions = {
          from: 'TK Fajar',
          to: this.member.email,
          subject: this.subject,
          html: this.body,
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
          }
          console.log('Message sent: %s', info.messageId);
        });
      });
    }
  }

  async greeting() {
    const ejs_string = fs.readFileSync(`${__dirname}/GreetingMail.ejs`, {
      encoding: 'utf8',
    });
    const template = ejs.compile(ejs_string, {});

    this.body = template({
      member: this.member,
      react_url: config[env].reactUrl,
      password: this.password,
    });
    await this.send();
  }
}

module.exports = MyMailer;
