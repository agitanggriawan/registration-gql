const casual = require('casual');
const bcrypt = require('bcrypt');

exports.seed = function (knex) {
  const data = [
    {
      uid: casual.uuid,
      nama_lengkap: 'Agit Anggriawan',
      email: 'admin@tkfajar.com',
      password: bcrypt.hashSync('haha', bcrypt.genSaltSync(10)),
      role: 'ADMIN',
      aktif: true,
    },
    {
      uid: casual.uuid,
      nama_lengkap: 'John Doe',
      email: 'user@tkfajar.com',
      password: bcrypt.hashSync('haha', bcrypt.genSaltSync(10)),
      role: 'USER',
      aktif: true,
    },
  ];

  return knex('pengguna')
    .del()
    .then(function () {
      return knex('pengguna').insert(data);
    });
};
