const casual = require('casual');

exports.seed = function (knex) {
  const generateData = () => {
    let data = [];

    for (let i = 0; i < 50; i++) {
      const u = {
        pengguna_id: 1,
        tahun_ajaran_id: 1,
        nis: casual.unix_time.toString(),
        ksk: casual.unix_time.toString(),
        nama_lengkap: casual.full_name,
        nama_panggilan: casual.name,
        jenis_kelamin: casual.random_element(['Laki-laki', 'Perempuan']),
        tempat_lahir: casual.city,
        tanggal_lahir: casual.date((format = 'YYYY-MM-DD')),
        alamat_anak: casual.address,
        no_telepon_anak: casual.phone,
        warga_negara: casual.state,
        anak_ke: casual.random_element([1, 2, 3, 4]),
        jumlah_saudara: casual.random_element([0, 1, 2, 3, 4]),
        saudara_kandung: casual.random_element([0, 1, 2, 3, 4]),
        saudara_tiri: casual.random_element([0, 1, 2, 3, 4]),
        saudara_angkat: casual.random_element([0, 1, 2, 3, 4]),
        status_anak: casual.random_element([
          'Anak Yatim',
          'Anak Piatu',
          'Anak Yatim Piatu',
          'Tidak Yatim Piatu',
        ]),
        bahasa_sehari: casual.country,
        warga_negara: casual.state,
        agama: casual.random_element(['Islam', 'Kristen', 'Hindu', 'Buddha']),
        kelainan_jasmani: '',
        asal_sekolah: casual.company_name,
        nama_ayah: casual.full_name,
        tempat_lahir_ayah: casual.city,
        tanggal_lahir_ayah: casual.date((format = 'YYYY-MM-DD')),
        agama_ayah: casual.random_element([
          'Islam',
          'Kristen',
          'Hindu',
          'Buddha',
        ]),
        pendidikan_tertinggi_ayah: casual.random_element([
          'SD',
          'SMP',
          'SMA / SMK',
          'D1',
          'D3',
          'D4',
          'S1',
          'S2',
          'S3',
        ]),
        pekerjaan_jabatan_ayah: casual.catch_phrase,
        penghasilan_ayah: casual.integer((from = 5000000), (to = 10000000)),
        warga_negara_ayah: casual.state,
        alamat_ayah: casual.address,
        telepon_rumah_ayah: casual.phone,
        telepon_kantor_ayah: casual.phone,
        aktif: true,
      };

      data.push(u);
    }

    return data;
  };

  return knex('siswa')
    .del()
    .then(function () {
      return knex('siswa').insert(generateData());
    });
};
