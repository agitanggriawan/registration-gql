const casual = require('casual');

exports.seed = function (knex) {
  const data = [
    {
      tid: casual.uuid,
      periode_awal: 2020,
      periode_akhir: 2021,
      semester: 1,
      aktif: true,
    },
    {
      tid: casual.uuid,
      periode_awal: 2020,
      periode_akhir: 2021,
      semester: 2,
      aktif: false,
    },
  ];

  return knex('tahun_ajaran')
    .del()
    .then(function () {
      return knex('tahun_ajaran').insert(data);
    });
};
