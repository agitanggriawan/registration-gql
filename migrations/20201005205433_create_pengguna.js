exports.up = async (knex) => {
  await knex.schema.createTable('pengguna', (t) => {
    t.increments('id').primary();
    t.string('uid').notNullable();
    t.string('nama_lengkap').notNullable();
    t.string('email').notNullable();
    t.string('password').notNullable();
    t.string('role').notNullable();
    t.boolean('aktif').default(false);
    t.timestamps(true, true);
  });
};

exports.down = async (knex) => {
  await knex.schema.dropTable('pengguna');
};
