exports.up = async (knex) => {
  await knex.schema.createTable('tahun_ajaran', (t) => {
    t.increments('id').primary();
    t.string('tid').notNullable();
    t.integer('periode_awal').notNullable();
    t.integer('periode_akhir').notNullable();
    t.integer('semester').notNullable();
    t.boolean('aktif').notNullable().default(false);
    t.timestamps(true, true);
  });
};

exports.down = async (knex) => {
  await knex.schema.dropTable('tahun_ajaran');
};
