exports.up = async (knex) => {
  await knex.schema.createTable('siswa', (t) => {
    t.increments('id').primary();
    t.integer('pengguna_id')
      .references('id')
      .inTable('pengguna')
      .notNullable()
      .index();
    t.integer('tahun_ajaran_id')
      .references('id')
      .inTable('tahun_ajaran')
      .notNullable()
      .index();
    t.string('nis').unique();
    t.string('ksk').notNullable();
    t.string('nama_lengkap').notNullable(); // I
    t.string('nama_panggilan');
    t.string('jenis_kelamin').notNullable();
    t.string('tempat_lahir').notNullable();
    t.date('tanggal_lahir').notNullable();
    t.string('alamat_anak').notNullable();
    t.string('no_telepon_anak');
    t.integer('anak_ke').notNullable();
    t.integer('jumlah_saudara').default(0);
    t.integer('saudara_kandung').default(0);
    t.integer('saudara_tiri').default(0);
    t.integer('saudara_angkat').default(0);
    t.string('status_anak').notNullable();
    t.string('bahasa_sehari').notNullable();
    t.string('warga_negara').notNullable();
    t.string('agama').notNullable();
    t.string('kelainan_jasmani');
    t.string('asal_sekolah');
    t.string('nama_ayah'); // II
    t.string('tempat_lahir_ayah');
    t.date('tanggal_lahir_ayah');
    t.string('agama_ayah');
    t.string('pendidikan_tertinggi_ayah');
    t.string('pekerjaan_jabatan_ayah');
    t.double('penghasilan_ayah');
    t.string('warga_negara_ayah');
    t.string('alamat_ayah');
    t.string('telepon_rumah_ayah');
    t.string('telepon_kantor_ayah');
    t.string('nama_wali'); // III
    t.string('tempat_lahir_wali');
    t.date('tanggal_lahir_wali');
    t.string('agama_wali');
    t.string('pendidikan_tertinggi_wali');
    t.string('pekerjaan_jabatan_wali');
    t.double('penghasilan_wali');
    t.string('warga_negara_wali');
    t.string('alamat_wali');
    t.string('telepon_rumah_wali');
    t.string('telepon_kantor_wali');
    t.string('status_tinggal_anak'); // IV
    t.double('jarak_rumah_ke_sekolah');
    t.string('transportasi_ke_sekolah');
    t.string('bakat_anak');
    t.string('golongan_darah_anak');
    t.string('imunisasi');
    t.string('histori_penyakit');
    t.text('keterangan_pembayaran');
    t.boolean('aktif').default(false);
    t.boolean('status_pembayaran').default(false);
    t.timestamps(true, true);
  });
};

exports.down = async (knex) => {
  await knex.schema.dropTable('siswa');
};
