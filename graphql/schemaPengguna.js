const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    authenticate(email: String!, password: String!): Pengguna
    pengguna(role: Roles!): [Pengguna]
    findPenggunaByUid(uid: String!): Pengguna
    findPenggunaByEmail(email: String!): Pengguna
  }

  extend type Mutation {
    createPengguna(data: InPengguna!): Pengguna
    updatePengguna(
      uid: String!
      nama_lengkap: String!
      email: String!
    ): Pengguna
    verifikasiPengguna(uid: String!): Pengguna
  }

  type Pengguna {
    id: ID!
    uid: String!
    token: String
    nama_lengkap: String!
    email: String!
    role: String!
    aktif: Boolean!
    created_at: DateTime
    updated_at: DateTime
  }

  input InPengguna {
    nama_lengkap: String!
    email: String!
    password: String!
    role: Roles!
  }
`;

module.exports = typeDefs;
