const moment = require('moment');
const { Siswa, TahunAjaran, Pengguna } = require('../models');

const resolvers = {
  Query: {
    siswa: async (_, args) => {
      console.log('==> Accessing siswa');

      return Siswa.query()
        .where(
          'tahun_ajaran_id',
          TahunAjaran.query()
            .select('id')
            .where({
              aktif: true,
            })
            .first()
        )
        .orderBy('nama_lengkap', 'ASC');
    },
    checkDataRegister: async (_, args) => {
      console.log('==> Accessing checkDataRegister', args.uid);

      return Siswa.query()
        .where(
          'tahun_ajaran_id',
          TahunAjaran.query()
            .select('id')
            .where({
              aktif: true,
            })
            .first()
        )
        .andWhere(
          'pengguna_id',
          Pengguna.query().select('id').where('uid', args.uid).first()
        )
        .first();
    },
    getDashboard: async () => {
      console.log('==> Accessing getDashboard');

      try {
        const data = await Siswa.query().where(
          'tahun_ajaran_id',
          TahunAjaran.query()
            .select('id')
            .where({
              aktif: true,
            })
            .first()
        );
        // var years = moment().diff(data[0].tanggal_lahir, 'years');
        const fourTillFive = data.filter(
          (x) =>
            moment().diff(x.tanggal_lahir, 'years') >= 4 &&
            moment().diff(x.tanggal_lahir, 'years') <= 5
        );
        const fiveTillSix = data.filter(
          (x) =>
            moment().diff(x.tanggal_lahir, 'years') > 5 &&
            moment().diff(x.tanggal_lahir, 'years') <= 6
        );

        return {
          total_pendaftar: data.length,
          usia_4: fourTillFive.length,
          usia_5: fiveTillSix.length,
          total_jenis_kelamin: {
            laki_laki: data.filter((x) => x.jenis_kelamin === 'Laki-laki')
              .length,
            perempuan: data.filter((x) => x.jenis_kelamin === 'Perempuan')
              .length,
          },
        };
      } catch (error) {
        console.log('==> Error Accessing getDashboard', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    siswaById: async (_, args) => {
      console.log('==> Accessing siswaById');

      try {
        return Siswa.query().findById(args.id);
      } catch (error) {
        console.log('==> Error Accessing siswaById', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    siswaByNis: async (_, args) => {
      console.log('==> Accessing siswaByNis');

      try {
        return Siswa.query().findOne({ nis: args.nis });
      } catch (error) {
        console.log('==> Error Accessing siswaByNis', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
  },
  Mutation: {
    registrasiSiswa: async (_, args) => {
      console.log('==> Accessing registrasiSiswa');

      try {
        const user = await Pengguna.query()
          .findOne({ uid: args.uid })
          .select('id');
        const tahunAjaran = await TahunAjaran.query()
          .select('id')
          .where({
            aktif: true,
          })
          .first();

        args.data.tahun_ajaran_id = tahunAjaran.id;
        args.data.pengguna_id = user.id;
        delete args.data.uid;

        return Siswa.query().insert(args.data).returning('*');
      } catch (error) {
        console.log('==> Error Accessing registrasiSiswa', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    verificationSiswa: async (_, args) => {
      console.log('==> Accessing verificationSiswa');

      try {
        const aktif = args.status_pembayaran ? true : false;

        return Siswa.query().patchAndFetchById(args.id, {
          aktif,
          nis: args.nis,
          keterangan_pembayaran: args.keterangan_pembayaran,
          status_pembayaran: args.status_pembayaran,
        });
      } catch (error) {
        console.log('==> Error Accessing verificationSiswa', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    updateSiswa: async (_, args) => {
      console.log('==> Accessing updateSiswa');

      try {
        const siswa = await Siswa.query().findOne({ nis: args.nis });

        return siswa.$query().updateAndFetch(args.data);
      } catch (error) {
        console.log('==> Error Accessing updateSiswa', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
  },
};

module.exports = resolvers;
