const { merge } = require('lodash');
const { gql } = require('apollo-server-express');

/* IMPORT FOR SCHEMA */
const schemaTahunAjaran = require('./schemaTahunAjaran');
const schemaPengguna = require('./schemaPengguna');
const schemaSiswa = require('./schemaSiswa');

/* IMPORT FOR RESOLVER */
const resolverTahunAjaran = require('./resolverTahunAjaran');
const resolverPengguna = require('./resolverPengguna');
const resolverSiswa = require('./resolverSiswa');

const baseSchema = gql`
  scalar JSON
  scalar Date
  scalar DateTime
  scalar Upload
  scalar Double

  enum Roles {
    ADMIN
    USER
  }

  type Total {
    count: Int!
  }

  type Query {
    version: String
  }

  type Mutation {
    base: String
  }
`;

const schemas = [baseSchema, schemaTahunAjaran, schemaPengguna, schemaSiswa];
const resolvers = [resolverTahunAjaran, resolverPengguna, resolverSiswa];

module.exports = {
  typeDefs: schemas,
  resolvers: merge(resolvers),
};
