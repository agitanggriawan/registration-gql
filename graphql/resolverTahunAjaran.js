const { TahunAjaran } = require('../models');

const resolvers = {
  Query: {
    tahunAjaran: async () => {
      console.log('==> Accessing tahunAjaran');

      try {
        return TahunAjaran.query().orderBy('id', 'DESC');
      } catch (error) {
        console.log('==> Error Accessing tahunAjaran', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    findTahunAjaranByTid: async (_, args) => {
      console.log('==> Accessing findTahunAjaranByTid');
      console.log('==> args', args);

      try {
        return TahunAjaran.query().findOne({ tid: args.tid });
      } catch (error) {
        console.log('==> Error Accessing findTahunAjaranByTid', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    findTahunAjaranByPeriode: async (_, args) => {
      console.log('==> Accessing findTahunAjaranByPeriode');

      return TahunAjaran.query().findOne({
        periode_awal: args.data.periode_awal,
        periode_akhir: args.data.periode_akhir,
        semester: args.data.semester,
      });
    },
  },
  Mutation: {
    createTahunAjaran: async (_, args) => {
      console.log('==> Accessing createTahunAjaran');

      return TahunAjaran.query().insert({ ...args.data, aktif: false });
    },
    updateTahunAjaran: async (_, args) => {
      console.log('==> Accessing updateTahunAjaran');

      return TahunAjaran.query()
        .patch({
          periode_awal: args.data.periode_awal,
          periode_akhir: args.data.periode_akhir,
          semester: args.data.semester,
        })
        .where({ tid: args.tid })
        .returning('*')
        .first();
    },
  },
};

module.exports = resolvers;
