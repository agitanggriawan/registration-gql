const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    tahunAjaran: [TahunAjaran]
    findTahunAjaranByTid(tid: String!): TahunAjaran
    findTahunAjaranByPeriode(data: InTahunAjaran!): TahunAjaran
  }

  extend type Mutation {
    createTahunAjaran(data: InTahunAjaran!): TahunAjaran
    updateTahunAjaran(tid: String!, data: InTahunAjaran!): TahunAjaran
  }

  type TahunAjaran {
    id: ID!
    tid: String!
    periode_awal: Int!
    periode_akhir: Int!
    semester: Int!
    aktif: Boolean
    created_at: DateTime
    updated_at: DateTime
  }

  input InTahunAjaran {
    periode_awal: Int!
    periode_akhir: Int!
    semester: Int!
  }
`;

module.exports = typeDefs;
