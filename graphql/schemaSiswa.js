const { gql } = require('apollo-server-express');

const typeDefs = gql`
  extend type Query {
    siswa: [Siswa]
    checkDataRegister(uid: String!): Siswa
    getDashboard: Dashboard
    siswaById(id: ID!): Siswa
    siswaByNis(nis: String!): Siswa
  }

  extend type Mutation {
    registrasiSiswa(data: InSiswa!, uid: String!): Siswa
    verificationSiswa(
      id: ID!
      nis: String!
      keterangan_pembayaran: String
      status_pembayaran: Boolean
    ): Siswa
    updateSiswa(data: InSiswa, nis: String!): Siswa
  }

  type Siswa {
    id: ID!
    tahun_ajaran_id: ID!
    pengguna_id: ID!
    nis: String
    ksk: String!
    nama_lengkap: String!
    nama_panggilan: String
    jenis_kelamin: String!
    tempat_lahir: String!
    tanggal_lahir: Date!
    alamat_anak: String!
    no_telepon_anak: String
    anak_ke: Int!
    jumlah_saudara: Int
    saudara_kandung: Int
    saudara_tiri: Int
    saudara_angkat: Int
    status_anak: String!
    bahasa_sehari: String!
    warga_negara: String!
    agama: String!
    kelainan_jasmani: String
    asal_sekolah: String
    nama_ayah: String
    tempat_lahir_ayah: String
    tanggal_lahir_ayah: Date
    agama_ayah: String
    pendidikan_tertinggi_ayah: String
    pekerjaan_jabatan_ayah: String
    penghasilan_ayah: Double
    warga_negara_ayah: String
    alamat_ayah: String
    telepon_rumah_ayah: String
    telepon_kantor_ayah: String
    nama_wali: String
    tempat_lahir_wali: String
    tanggal_lahir_wali: Date
    agama_wali: String
    pendidikan_tertinggi_wali: String
    pekerjaan_jabatan_wali: String
    penghasilan_wali: Double
    warga_negara_wali: String
    alamat_wali: String
    telepon_rumah_wali: String
    telepon_kantor_wali: String
    status_tinggal_anak: String
    jarak_rumah_ke_sekolah: Double
    transportasi_ke_sekolah: String
    bakat_anak: String
    golongan_darah_anak: String
    imunisasi: String
    histori_penyakit: String
    aktif: Boolean
    status_pembayaran: Boolean
    keterangan_pembayaran: String
    created_at: DateTime
    updated_at: DateTime
  }

  input InSiswa {
    uid: String
    ksk: String!
    nama_lengkap: String!
    nama_panggilan: String
    jenis_kelamin: String!
    tempat_lahir: String!
    tanggal_lahir: Date!
    alamat_anak: String!
    no_telepon_anak: String
    anak_ke: Int!
    jumlah_saudara: Int
    saudara_kandung: Int
    saudara_tiri: Int
    saudara_angkat: Int
    status_anak: String!
    bahasa_sehari: String!
    warga_negara: String!
    agama: String!
    kelainan_jasmani: String
    asal_sekolah: String
    nama_ayah: String
    tempat_lahir_ayah: String
    tanggal_lahir_ayah: Date
    agama_ayah: String
    pendidikan_tertinggi_ayah: String
    pekerjaan_jabatan_ayah: String
    penghasilan_ayah: Double
    warga_negara_ayah: String
    alamat_ayah: String
    telepon_rumah_ayah: String
    telepon_kantor_ayah: String
    nama_wali: String
    tempat_lahir_wali: String
    tanggal_lahir_wali: Date
    agama_wali: String
    pendidikan_tertinggi_wali: String
    pekerjaan_jabatan_wali: String
    penghasilan_wali: Double
    warga_negara_wali: String
    alamat_wali: String
    telepon_rumah_wali: String
    telepon_kantor_wali: String
    status_tinggal_anak: String
    jarak_rumah_ke_sekolah: Double
    transportasi_ke_sekolah: String
    bakat_anak: String
    golongan_darah_anak: String
    imunisasi: String
    histori_penyakit: String
  }

  type Dashboard {
    total_pendaftar: Int
    usia_4: Int
    usia_5: Int
    total_jenis_kelamin: JenisKelamin
  }

  type JenisKelamin {
    laki_laki: Int
    perempuan: Int
  }
`;

module.exports = typeDefs;
