const { ApolloError } = require('apollo-server-express');
const { result } = require('lodash');
const { Pengguna } = require('../models');
const { generateToken } = require('../utils');
const MyMailer = require('../utils/MyMailer');

const resolvers = {
  Query: {
    authenticate: async (_, args) => {
      console.log('==> Accessing authenticate');
      console.log('==> args', args);

      try {
        const user = await Pengguna.query().findOne({
          email: args.email,
        });

        if (user && user.checkPassword(args.password)) {
          const data = {
            id: user.id,
            email: user.email,
            nama_lengkap: user.nama_lengkap,
            role: user.role,
          };
          const token = generateToken(data);
          user.token = token;

          return user;
        }

        throw new ApolloError(
          'Email atau Password tidak sesuai',
          'INTERNAL_SERVER_ERROR',
          null
        );
      } catch (error) {
        console.log('==> Error Accessing authenticate', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    pengguna: async (_, args) => {
      console.log('==> Accessing pengguna');

      return Pengguna.query().where({ role: args.role });
    },
    findPenggunaByUid: async (_, args) => {
      console.log('==> Accessing findPenggunaByUid');

      return Pengguna.query().findOne({ uid: args.uid });
    },
    findPenggunaByEmail: async (_, args) => {
      console.log('==> Accessing findPenggunaByEmail');

      return Pengguna.query().findOne({ email: args.email });
    },
  },
  Mutation: {
    createPengguna: async (_, args) => {
      console.log('==> Accessing createPengguna');
      console.log('==> args', args);

      try {
        const result = await Pengguna.query().insert(args.data);

        if (result) {
          const sendMail = new MyMailer(
            result,
            'Pendaftaran Anggota',
            args.data.password
          );
          await sendMail.greeting();
        }

        return result;
      } catch (error) {
        console.log('==> Error Accessing createPengguna', null);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    updatePengguna: async (_, args) => {
      console.log('==> Accessing updatePengguna');

      try {
        return Pengguna.query()
          .patch({
            nama_lengkap: args.nama_lengkap,
            email: args.email,
          })
          .where({ uid: args.uid })
          .returning('*')
          .first();
      } catch (error) {
        console.log('==> Error Accessing updatePengguna', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
    verifikasiPengguna: async (_, args) => {
      console.log('==> Accessing verifikasiPengguna');

      try {
        return Pengguna.query()
          .patch({
            aktif: true,
          })
          .where({ uid: args.uid })
          .returning('*')
          .first();
      } catch (error) {
        console.log('==> Error Accessing verifikasiPengguna', error);
        throw new ApolloError(error, 'INTERNAL_SERVER_ERROR', null);
      }
    },
  },
};

module.exports = resolvers;
