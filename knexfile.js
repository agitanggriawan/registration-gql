module.exports = {
  local: {
    client: 'postgresql',
    connection: {
      database: 'registration_local',
      user: 'postgres',
      password: 'password',
      host: 'localhost',
      port: 5432,
    },
    pool: {
      min: 10,
      max: 100,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },
};
