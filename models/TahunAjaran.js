const casual = require('casual');
const BaseModel = require('./BaseModel');

class TahunAjaran extends BaseModel {
  static get tableName() {
    return 'tahun_ajaran';
  }

  $beforeInsert() {
    this.tid = casual.uuid;
  }
}

module.exports = TahunAjaran;
