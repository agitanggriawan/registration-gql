const bcrypt = require('bcrypt');
const casual = require('casual');
const BaseModel = require('./BaseModel');

class Pengguna extends BaseModel {
  static get tableName() {
    return 'pengguna';
  }

  async $beforeInsert(args) {
    console.log('asdasd', this);
    this.uid = casual.uuid;
    this.password = bcrypt.hashSync(this.password, bcrypt.genSaltSync(10));
    this.aktif = this.role === 'ADMIN' ? true : false;

    return true;
  }

  checkPassword(password) {
    if (password) {
      const result = bcrypt.compareSync(password, this.password);

      return result;
    }
    return false;
  }
}

module.exports = Pengguna;
